using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : Entity
{
    // active variables
    int pointsToSpend = 0;
    int experiencePoints = 0;
    bool holdAngle = false;
    Interactable currentInteractable;

    // components
    public GameObject cameraGO;
    PlayerCamera playerCam;

    // Start is called before the first frame update
    void Start()
    {
        playerCam = Camera.main.GetComponent<PlayerCamera>();

        if (photonView.IsMine)
        {
            Camera.main.GetComponent<PlayerCamera>().SetPlayer(this);
            stats.name = PhotonNetwork.NickName;
            stats.level = 0;
        }

        EntityStart(this);
    }

    // Update is called once per frame
    void Update()
    {
        EntityUpdate();

        if (dead)
        {
            if (!hasReportedDeath)
            {
                GameMaster.Instance.RespawnLocalPlayer();
                hasReportedDeath = true;
            }
        }

        if (photonView.IsMine)
        {
            // shoot
            if (Input.GetButton("Fire1"))
            {
                Weapon.RPCData data = weapon.Shoot(gameObject, Projectile.BulletAggression.FRIENDLY);
                if (data.successful)
                {
                    photonView.RPC("RPCShoot", RpcTarget.Others, data.crit, data.rotOffset);
                }
            }

            // reload
            if (Input.GetButton("Reload"))
            {
                weapon.Reload();
            }

            // move
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            bool jump = Input.GetButtonDown("Jump");
            bool interact = Input.GetButtonDown("Interact");
            // todo: add an indicator for this
            if (Input.GetButtonDown("HoldAngle"))
            {
                holdAngle = !holdAngle;
            }

            Move(horizontal, jump, holdAngle);

            // todo: add an indicator for when you can interact
            if (interact)
            {
                if (currentInteractable)
                {
                    currentInteractable.Interact(this);
                }
            }

            // todo: exp and levelling pass
            if (experiencePoints >= (stats.level + 1) * 2)
            {
                experiencePoints -= (stats.level + 1) * 2;
                stats.level += 1;
                Debug.Log("level up increasing points to spend");
                pointsToSpend += 1;
            }

            if (pointsToSpend > 0)
            {
                int value = 1;
                // match stat increases to number keys for now
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    weapon.increaseStat("dam", value);
                    pointsToSpend--;
                }
                if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    weapon.increaseStat("rpm", value);
                    pointsToSpend--;
                }
                if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    weapon.increaseStat("mag", value);
                    pointsToSpend--;
                }
                if (Input.GetKeyDown(KeyCode.Alpha4))
                {
                    weapon.increaseStat("sta", value);
                    pointsToSpend--;
                }
                if (Input.GetKeyDown(KeyCode.Alpha5))
                {
                    weapon.increaseStat("rng", value);
                    pointsToSpend--;
                }
            }

            if (weapon.level > 0)
            {
                if (weapon.level % 10 == 0)
                {
                    Weapon.WeaponStats newStats = gwm.evolveWeapon(weapon.stats);
                    // this tomfoolery keeps our visible stats, while taking their invisible stats
                    // todo: make a custom transfer type to make this cleaner or smthn
                    newStats.dam = weapon.stats.dam;
                    newStats.rpm = weapon.stats.rpm;
                    newStats.mag = weapon.stats.mag;
                    newStats.sta = weapon.stats.sta;
                    newStats.rng = weapon.stats.rng;
                    weapon.stats = newStats;
                    weapon.InitializeWeapon(gameObject);
                    weapon.level++;
                    Debug.Log("weapon evolver increasing points to spend");
                    pointsToSpend++;
                }

            }

            playerCam.UpdateUI(weapon, stats.level, experiencePoints, pointsToSpend);
        }
    }

    public void SetInteractable(Interactable interactable)
    {
        currentInteractable = interactable;
    }

    public void GrantXP(int xp)
    {
        // todo: xp gain popup
        experiencePoints += xp;
    }

    public new void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        EntityOnPhotonSerializeView(stream, info);

        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(stats.level);
            stream.SendNext(experiencePoints);
        }
        else
        {
            // Network player, receive data
            stats.level = (int)stream.ReceiveNext();
            this.experiencePoints = (int)stream.ReceiveNext();
        }
    }
}
