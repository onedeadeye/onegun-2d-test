﻿using UnityEngine;

[System.Serializable]

// This class contains the definition for the values assigned to a weapon type

public class Weapon
{
    private GameObject host;

    public class WeaponCalcs
    {
        // pre-calculated values
        public float calcDam;
        public float calcRPMDelay;
        public int calcMag;
        public float calcStaOffset;
        public float calcRng;
        public float calcSpd;
    }

    // "live" values
    public float stability;
    public int magazine;
    public float nextTimeToFire;
    public int level = 0;

    [System.Serializable]
    public class WeaponStats
    {
        // identity stuff
        public string name;
        public string desc;
        public int tier;

        // visible stats
        public int dam; // measured in HP
        public int rpm; // measured in RPM
        public int mag; // measured in bullets
        public int sta; // measured in percents of a full crit
        public int rng; // measured in units

        // hidden stats
        public float f_damMulti;
        public float f_baseDam;
        public float f_damFloor; // % of total damage dropoff can fall to. double distance is 1/2 damage.

        public float f_critMulti;

        public float f_rpmMulti;
        public int i_baseRpm;

        public float f_magMulti;
        public int i_baseMag;

        public float f_staMulti;
        public int i_baseSta;
        public float f_staGainMulti; // number of seconds to full crit

        public float f_rngMulti;
        public float f_baseRng;

        public float f_spdMulti;
        public float f_baseSpd;

        public float f_staFloor;
    }

    public WeaponStats stats = new WeaponStats();
    public WeaponCalcs calcs = new WeaponCalcs();

    public void InitializeWeapon(GameObject host)
    {
        this.host = host;
        calcStats();
        // todo: make this transfer the percentage of reserves you had before
        Reload();
        stability = stats.f_staFloor;
    }

    // cool functions
    public void modifyAccuracy(bool grounded)
    {
        if (grounded)
        {
            // todo: that 100 is a magic number
            stability += (100 * (1 / stats.f_staGainMulti * Time.deltaTime));
        }
        else
        {
            stability -= (100 * (1 / stats.f_staGainMulti * Time.deltaTime));
        }
    }

    public class RPCData
    {
        public RPCData(bool successful)
        {
            this.successful = successful;
        }
        public RPCData(bool crit, float rotOffset)
        {
            this.crit = crit;
            this.rotOffset = rotOffset;
            this.successful = true;
        }

        public bool successful;
        public bool crit;
        public float rotOffset;
    }

    // returns a ReturnData class for whether firing was stopped by an empty mag,
    // it's not fireTime, or a successful shoot.
    public RPCData Shoot(GameObject host, Projectile.BulletAggression aggro)
    {
        if (canFire())
        {
            if (magazine > 0)
            {
                nextTimeToFire = Time.time + calcs.calcRPMDelay;

                bool crit = false;
                if (Random.Range(1, 100) <= stability)
                {
                    crit = true;
                }

                float damage = CalculateDamage(crit);

                Projectile projectile = Object.Instantiate(GameMaster.gwmInstance.bulletProjectile).GetComponent<Projectile>();
                GameObject muzzlePoint = host.GetComponent<Entity>().muzzlePoint;
                projectile.transform.SetPositionAndRotation(muzzlePoint.transform.position, muzzlePoint.transform.rotation);

                // todo: add "zero stability bloom angle" stat
                // todo: LOL thats a magic number and a half
                // todo: ...magic number and negative 45/100ths
                float stabilityMaxOffset = (-0.3f * stability) + 30;
                float rotOffset = Random.Range(stabilityMaxOffset, -stabilityMaxOffset);
                projectile.transform.Rotate(Vector3.forward * rotOffset);
                projectile.SetupProjectile(stats, calcs, damage, aggro, host.GetComponent<Entity>(), crit);

                // todo: fix the stability equation
                stability -= calcs.calcStaOffset;

                magazine -= 1;

                // todo: muzzle flash would go here too
                return new RPCData(crit, rotOffset);
            }
            // fail for empty mag
            return new RPCData(false);
        }
        // fail for not fire time
        return new RPCData(false);
    }

    public void Shoot(GameObject host, Projectile.BulletAggression aggro, bool crit, float rotOffset)
    {
        Projectile projectile = Object.Instantiate(GameMaster.gwmInstance.bulletProjectile).GetComponent<Projectile>();
        GameObject muzzlePoint = host.GetComponent<Entity>().muzzlePoint;
        projectile.transform.SetPositionAndRotation(muzzlePoint.transform.position, muzzlePoint.transform.rotation);

        // todo: add "zero stability bloom angle" stat
        // todo: LOL thats a magic number and a half
        // todo: ...magic number and negative 45/100ths
        projectile.transform.Rotate(Vector3.forward * rotOffset);
        projectile.SetupProjectile(calcs.calcSpd, aggro, crit);
    }

    public void Reload() => magazine = calcs.calcMag;

    public bool canFire() => Time.time >= nextTimeToFire;

    // normalizes live variables
    public void normalizeParams()
    {
        // critchance
        if (stability > 100f)
        {
            stability = 100f;
        }
        if (stability < stats.f_staFloor)
        {
            stability = stats.f_staFloor;
        }

        // reserves
        if (magazine > calcs.calcMag)
        {
            magazine = calcs.calcMag;
        }
        if (magazine < 0)
        {
            magazine = 0;
        }
    }

    // uncool functions
    public float CalculateDamage(bool crit)
    {
        float critMod = 1f;
        if (crit) { critMod = stats.f_critMulti; }
        return calcs.calcDam * critMod;
    }

    public void calcStats()
    {
        calcs.calcDam = stats.f_baseDam + (stats.dam * stats.f_damMulti);
        calcs.calcRPMDelay = (60 / (stats.i_baseRpm + (stats.rpm * stats.f_rpmMulti)));
        calcs.calcMag = (int)(stats.i_baseMag + (stats.mag * stats.f_magMulti));
        calcs.calcStaOffset = 100 - (stats.i_baseSta + (stats.sta * stats.f_staMulti));
        calcs.calcRng = stats.f_baseRng + (stats.rng * stats.f_rngMulti);
        calcs.calcSpd = stats.f_baseSpd + (stats.rng * stats.f_spdMulti);
    }

    public void increaseStat(string stat, int value)
    {
        var selectedStat = stats.GetType().GetField(stat);

        int oldValue = (int)selectedStat.GetValue(stats);

        selectedStat.SetValue(stats, oldValue += value);
        level++;
        calcStats();
    }
}
