using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGraphicSwitcher : MonoBehaviour
{
    public Player player;
    string[] weaponNames;

    void Start()
    {
        weaponNames = GameMaster.gwmInstance.weaponNames;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDecal(string weaponName)
    {
        if (player && weaponNames != null)
        {
            for (int i = 0; i < weaponNames.Length; i++)
            {
                if (weaponNames[i] == weaponName)
                {
                    DeactivateAllDecals();
                    transform.GetChild(i).gameObject.SetActive(true);
                }
            }
        }
        else
        {
            // we haven't latched on to the local player yet
        }
    }

    private void DeactivateAllDecals()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
