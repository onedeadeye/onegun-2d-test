using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EntityCanvas : MonoBehaviour
{
    public Text nameplate;
    public RectTransform healthOverlay;
    public GameObject speechBubbleRoot;
    public GameObject speechBubblePrefab;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateSpeechBubble(BaseEventData eventData)
    {

    }

    public void CreateSpeechBubble(string message)
    {
        GameObject speechBubble = GameObject.Instantiate(speechBubblePrefab, speechBubbleRoot.transform);
        speechBubble.transform.localPosition = Vector3.zero;
        speechBubble.transform.localEulerAngles = Vector3.zero;
        speechBubble.GetComponentInChildren<Text>().text = message;
        Destroy(speechBubble, 5f);
    }

    public void SetEntityVisibleName(int level, string name)
    {
        nameplate.text = (level + " | " + name);
    }

    public void UpdateEntityCanvas(float healthPercent)
    {
        healthOverlay.sizeDelta = new Vector2(healthPercent * 60, 5);
    }
}
