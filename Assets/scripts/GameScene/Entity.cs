using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Entity : MonoBehaviourPunCallbacks, IPunObservable
{
    public enum EntityAggression
    {
        PASSIVE,
        NEUTRAL,
        AGGRESSIVE
    }

    [System.Serializable]
    public class EntityStats
    {
        public string name;
        public int experienceValue;
        public float maxHealth;
        public int level;
        public float moveSpeed;
        public float jumpForce;
        public EntityAggression aggression;
        public string weaponName;
    }

    public EntityStats stats;
    public Weapon weapon;

    public Object self;
    public Rigidbody2D rb;
    public GameObject feet;
    public GameObject muzzlePoint;
    public LayerMask countsAsGround;
    public EntityCanvas entityCanvas;

    // live variables
    public float health = 20f;
    public bool dead = false;
    protected bool hasReportedDeath = false;
    public bool grounded = false;
    // todo: make sure all entities start facing right
    public bool facingRight = true;

    // outside references
    protected GameMaster gm;
    protected GameWeaponMaster gwm;

    // EntityStart needs to be called by all Entity implementations at the very beginning of their Start()
    protected void EntityStart(Object self)
    {
        gm = GameMaster.Instance;
        gwm = GameMaster.gwmInstance;

        weapon = new Weapon();
        weapon.stats = gwm.generateStats(stats.weaponName);
        weapon.InitializeWeapon(gameObject);

        health = stats.maxHealth;

        this.self = self;
        rb = GetComponent<Rigidbody2D>();
    }

    // EntityUpdate must be called in the Update() of all Entity derivates
    protected void EntityUpdate()
    {
        weapon.modifyAccuracy(grounded);

        weapon.normalizeParams();

        // todo: BEEP BEEP MAGIC NUMBER
        Collider2D[] grounds = Physics2D.OverlapCircleAll(feet.transform.position, 0.2f, countsAsGround);
        grounded = (grounds.Length > 0) ? true : false;

        entityCanvas.UpdateEntityCanvas(health / stats.maxHealth);
        entityCanvas.SetEntityVisibleName(stats.level, stats.name);
    }

    // this should always be called from Updates
    protected void Move(float hor, bool jump, bool holdDirection)
    {
        if (dead)
        {
            return;
        }

        float move = 0f;
        move = hor * stats.moveSpeed;

        if (!holdDirection)
        {
            if (0 < move && !facingRight)
            {
                Flip();
            }
            else if (move < 0 && facingRight)
            {
                Flip();
            }
        }

        // todo: custom movement code
        rb.velocity = new Vector2(move, rb.velocity.y);

        if (jump && grounded)
        {
            grounded = false;
            rb.AddForce(Vector2.up * stats.jumpForce);
        }
    }

    protected void SetRotation()
    {

    }

    // protected void Aim(float hor, float ver, bool holdDirection)
    // {
    //     float aimAngle = (Mathf.Atan(ver / Mathf.Abs(hor))) * (180/Mathf.PI);

    //     if(!holdDirection) {
    //         muzzlePoint.transform.localEulerAngles = Vector3.zero;
    //     }
    // }

    public bool IsDead()
    {
        return dead;
    }

    [PunRPC]
    public void RPCShoot(bool crit, float rotOffset)
    {
        weapon.Shoot(gameObject, Projectile.BulletAggression.FRIENDLY, crit, rotOffset);
    }

    [PunRPC]
    public void Say(string message)
    {
        entityCanvas.CreateSpeechBubble(message);
        // todo: put the message in chat too
    }

    [PunRPC]
    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    protected void Flip()
    {
        facingRight = !facingRight;

        Vector3 rotation = transform.eulerAngles;
        rotation.y += 180;
        if (rotation.y % 360 == 0)
        {
            rotation.y = 0;
        }
        transform.eulerAngles = rotation;
        entityCanvas.transform.Rotate(Vector3.up * 180);
    }

    private void Die()
    {
        dead = true;
        GetComponentInChildren<SpriteRenderer>().color = Color.gray;
        rb.constraints = RigidbodyConstraints2D.None;
        Vector2 ragdollVector = new Vector2(2 * (facingRight ? 1 : -1), 1);
        float ragdollForce = 2f + Random.Range(-1f, 1f);
        rb.AddForce(ragdollVector * ragdollForce, ForceMode2D.Impulse);
    }

    [PunRPC]
    public void Respawn()
    {
        health = stats.maxHealth;
        dead = false;
        hasReportedDeath = false;
        GetComponentInChildren<SpriteRenderer>().color = Color.white;
        Vector3 newRot = Vector3.zero;
        newRot.x = 0f;
        newRot.z = 0f;
        newRot.y = transform.eulerAngles.y;
        transform.eulerAngles = newRot;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        weapon.Reload();
        if (!facingRight)
        {
            Flip();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        EntityOnPhotonSerializeView(stream, info);
    }

    public void EntityOnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this entity: send the others our data
            stream.SendNext(health);
            stream.SendNext(facingRight);
            stream.SendNext(stats.name);
        }
        else
        {
            // Network entity, receive data
            this.health = (float)stream.ReceiveNext();
            bool newFacingRight = (bool)stream.ReceiveNext();
            if (facingRight != newFacingRight)
            {
                Flip();
            }
            this.stats.name = (string)stream.ReceiveNext();
        }
    }
}
