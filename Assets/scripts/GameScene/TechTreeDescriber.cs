using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TechTreeDescriber : MonoBehaviour
{
    Text descriptionBox;

    // Start is called before the first frame update
    void Start()
    {
        descriptionBox = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDescribedName(string name)
    {
        Weapon.WeaponStats stats = GameMaster.gwmInstance.generateStats(name);
        descriptionBox.text = string.Format("{0}\n{1} DAM | {2} RPM | {3} MAG | {4} STA | {5} RANGE", stats.name, stats.dam, stats.rpm, stats.mag, stats.sta, stats.rng);
    }

    public void ResetDescriptionBox()
    {
        descriptionBox.text = "Weapon Name\nDAM/RPM/MAG/STA/RNG";
    }
}
