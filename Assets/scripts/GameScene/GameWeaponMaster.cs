using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWeaponMaster : MonoBehaviour
{
    private static string weaponDataName = "weaponData";
    GameMaster.CSVLoader wData = new GameMaster.CSVLoader(weaponDataName);

    // todo: hey jackass, its your job to keep this string array updated so the world keeps spinning
    // fixed 2/15/21: the world will now keep spinning on it's own
    public string[] weaponNames;
    private List<Weapon.WeaponStats> types = new List<Weapon.WeaponStats>();

    // todo: add a way to get the proper projectile for a weapon
    public GameObject bulletProjectile;

    // VFX i dont feel like putting somewhere more fitting
    public GameObject damageMarker;
    public GameObject impactMarker;

    void Awake()
    {
        wData.loadData();

        populateWeaponsLists();
    }

    // todo: what the hell is this
    public Weapon.WeaponStats GetStats()
    {
        return types[0];
    }

    public void populateWeaponsLists()
    {
        wData.checkValidity();

        // but you DO have to cut off the top two lines for THIS to work
        // todo: make this more tardproof
        ArrayList weapons = new ArrayList();

        foreach (string[] data in wData.splitData)
        {
            weapons.Add(data[0]);
        }

        weaponNames = new string[weapons.Count];

        int i = 0;
        foreach (string data in weapons)
        {
            weaponNames[i] = data;
            i++;
        }

        // this tomfoolery auto-populates the types list with all the weapon stat definitions for use when comparing for evos later
        // doesn't use reflection any more :( (but now it's a lot cleaner lol)
        foreach (string name in weaponNames)
        {
            types.Add(generateStats(name));
        }
    }

    public Weapon.WeaponStats generateStats(string target)
    {
        Weapon.WeaponStats result = new Weapon.WeaponStats();

        // identity stuff
        result.name = target;

        setWeaponData(ref result);

        return result;
    }

    private void setWeaponData(ref Weapon.WeaponStats result)
    {
        int index = wData.getRecordIndex(result.name);
        // stats start at index 1, as 0 is the name
        // index 1 is the weapon tier
        result.tier = wData.getData<int>(index, 1);

        // visible stats, index 2-6
        result.dam = wData.getData<int>(index, 2);
        result.rpm = wData.getData<int>(index, 3);
        result.mag = wData.getData<int>(index, 4);
        result.sta = wData.getData<int>(index, 5);
        result.rng = wData.getData<int>(index, 6);

        // hidden stats, index 7-20
        result.f_damMulti = wData.getData<float>(index, 7);
        result.f_baseDam = wData.getData<float>(index, 8);
        result.f_damFloor = wData.getData<float>(index, 9);

        result.f_critMulti = wData.getData<float>(index, 10);

        result.f_rpmMulti = wData.getData<float>(index, 11);
        result.i_baseRpm = wData.getData<int>(index, 12);

        result.f_magMulti = wData.getData<float>(index, 13);
        result.i_baseMag = wData.getData<int>(index, 14);

        result.f_staMulti = wData.getData<float>(index, 15);
        result.i_baseSta = wData.getData<int>(index, 16);
        result.f_staGainMulti = wData.getData<float>(index, 17);

        result.f_rngMulti = wData.getData<float>(index, 18);
        result.f_baseRng = wData.getData<float>(index, 19);

        result.f_spdMulti = wData.getData<float>(index, 20);
        result.f_baseSpd = wData.getData<float>(index, 21);

        result.f_staFloor = wData.getData<float>(index, 22);
    }

    public Weapon.WeaponStats evolveWeapon(Weapon.WeaponStats target)
    {
        // todo: make the magic happen
        // todo: make setting this to 100f not the way this works
        double candidateSimilarity = 100f;
        Weapon.WeaponStats candidateStats = target;

        // this little subroutine calculates "similarity values" for each of the five visible stats
        // to determine which weapon is the most logical next evolution. lowest number wins!
        // todo: fine tune the math, exponential increase may make it too "fidgety"
        // that magic number is a magic number, which could be useful as a parameter for tweaking once the
        // "tech tree" gets pretty big

        float magicNumber = 1f;

        foreach (Weapon.WeaponStats candidate in types)
        {
            // Only consider candidates of one tier higher.
            if (candidate.tier != target.tier + 1)
            {
                continue;
            }

            double similarity = 0;
            similarity += Math.Abs(Math.Pow((target.dam - candidate.dam), magicNumber));
            similarity += Math.Abs(Math.Pow((target.rpm - candidate.rpm), magicNumber));
            similarity += Math.Abs(Math.Pow((target.mag - candidate.mag), magicNumber));
            similarity += Math.Abs(Math.Pow((target.sta - candidate.sta), magicNumber));
            similarity += Math.Abs(Math.Pow((target.rng - candidate.rng), magicNumber));
            Debug.Log("candidate " + candidate.name + " value " + similarity);

            if (similarity < candidateSimilarity)
            {
                candidateSimilarity = similarity;
                candidateStats = candidate;
            }
        }

        return candidateStats;
    }
}
