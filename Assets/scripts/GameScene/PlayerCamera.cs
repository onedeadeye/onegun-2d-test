using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCamera : MonoBehaviour
{

    Camera cam;

    // UI components
    Canvas worldUI;
    Canvas screenUI;
    public GameObject centerCrit;
    RectTransform leftCrit;
    RectTransform rightCrit;
    public RectTransform targetIndicator;
    public GameObject weaponGraphicsHost;
    WeaponGraphicSwitcher wGS;
    public GameObject weaponStatsHost;
    public GameObject xpDisplay;
    public GameObject ammoDisplay;

    bool autoscroll = true;
    float yOffset = 3f;
    float zOffset = -10f;
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        wGS = weaponGraphicsHost.GetComponent<WeaponGraphicSwitcher>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player && autoscroll)
        {
            float height = 2f * cam.orthographicSize;
            float width = height * cam.aspect;

            if (autoscroll)
            {
                Vector3 newPos = player.transform.position;
                newPos.y = newPos.y + yOffset;
                newPos.z = zOffset;
                transform.position = newPos;
            }
        }
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        wGS.player = player;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public void UpdateUI(Weapon weapon, int level, int experience, int pointsAvailable)
    {
        wGS.SetDecal(weapon.stats.name);
        updateWeaponDisplays(weapon);
        updateStatDisplays(weapon.stats);
        updateXPDisplays(level, experience, pointsAvailable);
    }

    private void updateWeaponDisplays(Weapon weapon)
    {
        ammoDisplay.GetComponent<Text>().text = weapon.magazine + "/" + weapon.calcs.calcMag;
        targetIndicator.GetComponentInChildren<Text>().text = (int)weapon.stability + "%";
    }

    private void updateStatDisplays(Weapon.WeaponStats stats)
    {
        weaponStatsHost.transform.GetChild(0).GetComponent<Text>().text = stats.dam.ToString();
        weaponStatsHost.transform.GetChild(1).GetComponent<Text>().text = stats.rpm.ToString();
        weaponStatsHost.transform.GetChild(2).GetComponent<Text>().text = stats.mag.ToString();
        weaponStatsHost.transform.GetChild(3).GetComponent<Text>().text = stats.sta.ToString();
        weaponStatsHost.transform.GetChild(4).GetComponent<Text>().text = stats.rng.ToString();
    }

    private void updateXPDisplays(int level, int experience, int pointsAvailable)
    {
        xpDisplay.GetComponent<Text>().text = "level " + level.ToString() + " | " + experience.ToString() + "/" + ((level + 1) * 2).ToString();

        if (pointsAvailable > 0)
        {
            weaponStatsHost.transform.GetChild(5).gameObject.SetActive(true);
            weaponStatsHost.transform.GetChild(5).gameObject.GetComponent<Text>().text = pointsAvailable.ToString();
        }
        else
        {
            weaponStatsHost.transform.GetChild(5).gameObject.SetActive(false);
        }
    }

    public Enemy selectTarget()
    {
        RaycastHit hit;
        Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 100f);

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.GetComponent<Enemy>())
            {
                Enemy returnMe = hit.collider.gameObject.GetComponent<Enemy>();
                return returnMe;
            }
            return null;
        }
        return null;
    }
}
