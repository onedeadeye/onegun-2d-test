using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ChatHost : MonoBehaviourPunCallbacks
{
    PlayerCamera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<PlayerCamera>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    [PunRPC]
    public void PostChatMessage(string name, string message)
    {

    }

    public void PostChatBubble(string message)
    {
        GetComponentInChildren<InputField>().text = "";
        cam.GetPlayer().Say(message);
        cam.GetPlayer().photonView.RPC("Say", RpcTarget.Others, message);
    }
}
