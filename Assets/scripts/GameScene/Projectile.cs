using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviourPunCallbacks
{

    // eerily similar to entity aggression
    public enum BulletAggression
    {
        FRIENDLY,
        NEUTRAL,
        AGGRESSIVE
    }

    // stuff we get from the host
    Weapon.WeaponStats stats;
    Weapon.WeaponCalcs calcs;
    float damage;
    BulletAggression aggro = BulletAggression.NEUTRAL;
    Entity owner;
    Vector3 startPos;
    bool crit;
    bool isDummy = false;
    float dummySpeed;

    // live variables
    float travelDistance;

    public void SetupProjectile(Weapon.WeaponStats stats, Weapon.WeaponCalcs calcs, float damage, BulletAggression aggro, Entity owner, bool crit)
    {
        this.stats = stats;
        this.calcs = calcs;
        this.damage = damage;

        this.aggro = aggro;
        this.owner = owner;
        this.startPos = transform.position;
        this.crit = crit;

        if (crit)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    public void SetupProjectile(float dummySpeed, BulletAggression aggro, bool crit)
    {
        this.isDummy = true;
        this.dummySpeed = dummySpeed;
        this.aggro = aggro;
        this.crit = crit;

        if (crit)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // todo: make projectile lifetime a stat?
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (isDummy)
        {
            transform.Translate(Vector3.right * dummySpeed * Time.deltaTime, Space.Self);
        }
        else
        {
            transform.Translate(Vector3.right * calcs.calcSpd * Time.deltaTime, Space.Self);

            travelDistance = Vector3.Distance(transform.position, startPos);
        }
    }

    // todo: make projectile teaming based on entity aggression on both sides
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            // hit an enemy
            switch (aggro)
            {
                case BulletAggression.AGGRESSIVE:
                    // enemy bullet hit a enemy, do nothing
                    break;
                case BulletAggression.NEUTRAL:
                    // neutral bullet hit a enemy, do damage
                    HitEntity(collision.GetComponent<Enemy>());
                    break;
                case BulletAggression.FRIENDLY:
                    // ally bullet hit a enemy, do damage
                    HitEntity(collision.GetComponent<Enemy>());
                    break;
            }

        }
        else if (collision.CompareTag("Player"))
        {
            // hit a player
            switch (aggro)
            {
                case BulletAggression.AGGRESSIVE:
                    // enemy bullet hit a player, do damage
                    HitEntity(collision.GetComponent<Player>());
                    break;
                case BulletAggression.NEUTRAL:
                    // neutral bullet hit a player, do damage
                    HitEntity(collision.GetComponent<Player>());
                    break;
                case BulletAggression.FRIENDLY:
                    // ally bullet hit a player, do nothing
                    break;
            }
        }
        else
        {
            // hit a wall
            SpawnImpactMarker();
            Destroy(gameObject);
        }
    }

    private void HitEntity(Entity entity)
    {
        if (isDummy)
        {
            Destroy(gameObject);
            return;
        }

        if (!entity.IsDead())
        {
            float finalDamage = damage * CalculateDropoff();

            entity.TakeDamage(damage);
            entity.photonView.RPC("TakeDamage", RpcTarget.Others, damage);

            // XP logic used to be entity-side, changed to projectile
            if (entity.IsDead())
            {
                if (owner.GetType() == typeof(Player))
                {
                    Player player = (Player)owner.self;
                    player.GrantXP(entity.stats.experienceValue);
                }
            }

            SpawnDamageMarker(finalDamage);

            Destroy(gameObject);
        }
    }

    // returns the float percent damage is decreased by, given a distance to the target
    // todo: dropoff is pretty terrible
    private float CalculateDropoff()
    {
        // the range at which we can hit full damage
        float calcDist = travelDistance - calcs.calcRng;
        if (calcDist <= 0)
        {
            //we're within range, so return 1
            return 1f;
        }
        else
        {
            float remainder = calcs.calcRng / calcDist;
            return Mathf.Clamp(remainder, stats.f_damFloor, 1f);
        }
    }

    private void SpawnDamageMarker(float damage)
    {
        GameObject marker = Instantiate(GameMaster.gwmInstance.damageMarker);
        float result = Mathf.Round(damage * 10f) / 10f;
        marker.GetComponent<DamageMarker>().SetValues(result, crit);
        Vector3 pos2 = transform.position;
        pos2.y += 1f;
        marker.transform.position = pos2;
    }

    private void SpawnImpactMarker()
    {
        GameObject marker = Instantiate(GameMaster.gwmInstance.impactMarker);
        marker.transform.position = transform.position;
        marker.transform.forward = -transform.right;
        Destroy(marker, 0.1f);
    }
}
