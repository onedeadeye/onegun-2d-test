using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossSpawner : EnemySpawner
{

    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        int timeTillSpawn = (int)(nextSpawnTime - Time.time);

        int seconds = timeTillSpawn % 60;

        int minutes = (timeTillSpawn - seconds) / 60;

        text.text = (minutes + ":" + seconds);

        SpawnRoutine();
    }
}
