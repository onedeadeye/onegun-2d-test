using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    public enum InteractableType
    {
        TELEPORTER
    }

    public InteractableType interactableType;

    public GameObject[] cohorts;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            Debug.Log("player staying in collider");
            col.GetComponent<Player>().SetInteractable(this);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            Debug.Log("player leaving collider");
            col.GetComponent<Player>().SetInteractable(null);
        }
    }

    // returns true if interaction was successful
    public void Interact(Entity interactor)
    {
        switch (interactableType)
        {
            case InteractableType.TELEPORTER:
                Vector3 teleportPos = cohorts[0].transform.position;
                teleportPos.z = 0;
                interactor.transform.position = teleportPos;
                break;
            default:
                break;
        }
    }
}
