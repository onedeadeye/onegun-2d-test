using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class GameMaster : MonoBehaviourPunCallbacks
{
    public static GameMaster Instance { get; private set; }
    public static GameWeaponMaster gwmInstance { get; private set; }

    public GameObject SpawnPoint;
    public GameObject GamePanel;
    public GameObject MenuPanel;

    public bool menuOpen = false;

    public GameObject playerPrefab;

    GameObject localPlayer;

    float respawnDelay = 3f;

    public class CSVLoader
    {
        // loading stuff
        public string name;
        public TextAsset dataCSV;
        public char lineSeparator = '\n';
        public char fieldSeparator = ',';
        public string[][] splitData;

        public CSVLoader(string newName)
        {
            name = newName;
        }

        public void loadData()
        {
            dataCSV = Resources.Load<TextAsset>(name);
            string[] firstSplit = dataCSV.text.Split(lineSeparator);

            ArrayList entries = new ArrayList();
            foreach (string entry in firstSplit)
            {
                entries.Add(entry.Split(fieldSeparator));
            }

            splitData = new string[entries.Count][];

            int i = 0;

            foreach (string[] entry in entries)
            {
                splitData[i] = entry;
                i++;
            }
        }

        // this method pisses itself if there isn't a record that matches string name
        public int getRecordIndex(string name)
        {
            checkValidity();

            for (int i = 0; i < splitData.Length; i++)
            {
                if (splitData[i][0] == name)
                {
                    return i;
                }
            }

            // no match found!
            // todo: learn how exceptions work
            Debug.Log(name);
            throw new ApplicationException();
        }

        public T getData<T>(int record, int entry) where T : struct
        {
            checkValidity();

            return (T)Convert.ChangeType(splitData[record][entry], typeof(T));
        }

        public void checkValidity()
        {
            if (splitData == null)
            {
                loadData();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        localPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, SpawnPoint.transform.position, Quaternion.identity, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            menuOpen = !menuOpen;

            GamePanel.SetActive(!menuOpen);
            MenuPanel.SetActive(menuOpen);
        }
    }

    // singleton-er
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            gwmInstance = GetComponent<GameWeaponMaster>();
        }
    }

    public void RespawnLocalPlayer()
    {
        StartCoroutine("RespawnPlayer", localPlayer);
    }

    IEnumerator RespawnPlayer(GameObject player)
    {
        yield return new WaitForSeconds(respawnDelay);

        player.transform.position = SpawnPoint.transform.position;

        player.GetComponent<Entity>().Respawn();
        player.GetPhotonView().RPC("Respawn", RpcTarget.Others);
    }

    void LoadLevel()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
        }
        Debug.Log("PhotonNetwork : Loading Level");
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player other)
    {
        Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

            //LoadLevel();
        }
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player other)
    {
        Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

            //LoadLevel();
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }
}
