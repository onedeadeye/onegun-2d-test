using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Enemy : Entity
{
    public EnemySpawner spawner;

    // AI variables
    Entity target;
    public LayerMask sightLayerMask;

    [System.Serializable]
    public class EnemyStats
    {
        public float sightRadius;
        public float followRadius;
        public float minStability;
        public float jumpFrequency;
    }

    public EnemyStats enemyStats;

    // Start is called before the first frame update
    void Start()
    {
        // another magic string!
        EntityStart(this);
    }

    // Update is called once per frame
    void Update()
    {
        EntityUpdate();

        if (dead)
        {
            if (!hasReportedDeath)
            {
                Destroy(gameObject, 5f);
                if (spawner) { spawner.ReportDeath(gameObject); }
                hasReportedDeath = true;
            }
        }
        else
        {
            if (photonView.IsMine)
            {
                // AI routine goes here
                if (target)
                {
                    float xDist = target.transform.position.x - transform.position.x;
                    float yDist = target.transform.position.y - transform.position.y;
                    if (target.IsDead())
                    {
                        target = null;
                    }
                    else if (Vector2.Distance(transform.position, target.transform.position) > enemyStats.followRadius)
                    {
                        target = null;
                    }
                    else if (Mathf.Abs(xDist) > enemyStats.followRadius)
                    {
                        target = null;
                    }
                    else
                    {

                        float move = 0f;

                        float optimalRange = weapon.calcs.calcRng;

                        if (xDist > 0 && !facingRight) { Flip(); }
                        else if (xDist < 0 && facingRight) { Flip(); }

                        if (xDist < optimalRange && xDist > -optimalRange)
                        {
                            move = 0f;
                        }
                        else if (xDist > optimalRange)
                        {
                            move = 1f;
                        }
                        else if (xDist < optimalRange)
                        {
                            move = -1f;
                        }

                        float jumpResult = Random.Range(1, 100);

                        // todo: jumping enemies was cool when i did it,
                        // but they could easily "superjump"
                        bool jump = jumpResult < enemyStats.jumpFrequency;

                        Move(move, false, false);

                        if (weapon.stability > enemyStats.minStability)
                        {
                            if (weapon.canFire())
                            {
                                // if we're facing away from the player, don't bother shooting
                                if (xDist > 0 && !facingRight) { return; }
                                if (xDist < 0 && facingRight) { return; }

                                Weapon.RPCData data = weapon.Shoot(gameObject, Projectile.BulletAggression.AGGRESSIVE);
                                if (data.successful)
                                {
                                    photonView.RPC("RPCShoot", RpcTarget.Others, data.crit, data.rotOffset);
                                }
                                else
                                {
                                    weapon.Reload();
                                }
                            }
                        }
                    }
                }
                else
                {
                    Collider2D[] results = new Collider2D[10];
                    ContactFilter2D filter = new ContactFilter2D();
                    filter = filter.NoFilter();
                    int resultNum = Physics2D.OverlapCircle(transform.position, enemyStats.sightRadius, filter, results);

                    float closestDist = enemyStats.sightRadius;

                    Collider2D closestCol = this.GetComponent<Collider2D>();
                    foreach (Collider2D col in results)
                    {
                        if (col)
                        {
                            float distance = Vector2.Distance(col.transform.position, transform.position);

                            if (col.CompareTag("Player"))
                            {
                                if (distance < closestDist && !col.GetComponent<Entity>().IsDead())
                                {
                                    closestCol = col;
                                }
                            }
                        }
                    }

                    if (closestCol != this.GetComponent<Collider2D>())
                    {
                        float yDist = closestCol.transform.position.y - transform.position.y;

                        // todo: eviscerate this magic number
                        if (yDist < 5)
                        {
                            target = closestCol.GetComponent<Entity>();
                        }
                    }
                }
            }
        }
    }
}
