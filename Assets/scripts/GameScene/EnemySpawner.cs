using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemySpawner : MonoBehaviourPunCallbacks, IPunObservable
{
    public string enemyName;
    public bool infiniteSpawn = true;
    public float spawnDelay = 5f;
    public int MaxSpawns = 5;
    protected float nextSpawnTime = 0f;
    Vector3 spawnPosition;

    List<GameObject> spawns = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        spawnPosition = transform.position;
        spawnPosition.z = 0f;
    }

    void Update()
    {
        SpawnRoutine();
    }

    protected void SpawnRoutine()
    {
        if (infiniteSpawn)
        {
            if (Time.time > nextSpawnTime)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    // only the master does the spawning
                    if (spawns.Count < MaxSpawns)
                    {
                        GameObject spawn = PhotonNetwork.Instantiate(enemyName, spawnPosition, Quaternion.identity);
                        spawn.GetComponent<Enemy>().spawner = this;
                        spawns.Add(spawn);
                    }
                }
                else
                {
                    Debug.Log("Client player respected spawn time");
                }
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
    }

    public void ReportDeath(GameObject theDeparted)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            spawns.Remove(theDeparted);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(nextSpawnTime);
        }
        else
        {
            // Network player, receive data
            this.nextSpawnTime = (float)stream.ReceiveNext();
        }
    }
}
