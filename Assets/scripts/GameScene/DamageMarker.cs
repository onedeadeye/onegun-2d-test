using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageMarker : MonoBehaviour
{
    public Text text;
    public Rigidbody rb;

    public void SetValues(float damage, bool crit)
    {
        Color color;
        color = crit ? color = Color.red : color = Color.cyan;
        text.text = damage.ToString();
        text.color = color;
        Vector3 randomOffset = new Vector3();
        randomOffset.x = Random.Range(-0.4f, 0.4f);
        randomOffset.y = Random.Range(0f, 0.5f);
        randomOffset.z = Random.Range(-0.4f, 0.4f);
        rb.AddForce((Vector3.up + randomOffset) * 200f);
        Destroy(gameObject, 2f);
    }
}
